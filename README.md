## Disney Clone with GraphCMS

This repo is to build a Disney Clone with GraphCMS.

## Creating graphcms account

First, you need to create your GraphCMS Account at https://app.graphcms.com/.

## Add a `.env` file

After creating the account and loading the assets, add a `.env` file with the following from Settings -> API access endpoints:

GRAPH_CMS_TOKEN={your_token}
ENDPOINT={your_endpoint}

## Install packages 

Open the cloned code in any editor (VS code), run `npm i` to install the packages.

## run

To run the development server, trigger the command in terminal -> `npm run dev`

## Access the application
Application is accessible at http://localhost:3000
